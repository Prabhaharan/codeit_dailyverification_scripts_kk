package application

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords



import MobileBuiltInKeywords as Mobile
import WSBuiltInKeywords as WS
import WebUiBuiltInKeywords as WebUI

public class ProgramWizard {
	@Keyword
	public String deleteProgram(int existingProgramsCount){
		def deleteProgramStatus=""
		try{
			for(int deleteProgramCount=1;deleteProgramCount<=existingProgramsCount;deleteProgramCount++){
				WebUI.verifyElementPresent(findTestObject('Application/EnrollmentAndCareteamWizard/ProgramWizard/EACW_PW_VerticalElipsisIcon'), 30, FailureHandling.STOP_ON_FAILURE)
				WebUI.click(findTestObject('Application/EnrollmentAndCareteamWizard/ProgramWizard/EACW_PW_VerticalElipsisIcon'), FailureHandling.STOP_ON_FAILURE)
				WebUI.delay(3)
				WebUI.click(findTestObject('Application/EnrollmentAndCareteamWizard/ProgramWizard/EACW_PW_DeleteIcon'), FailureHandling.STOP_ON_FAILURE)
				WebUI.delay(2)
				WebUI.verifyElementPresent(findTestObject('Application/EnrollmentAndCareteamWizard/ProgramWizard/EACW_PW_DeleteProgramPopupDeleteButton'), 30, FailureHandling.STOP_ON_FAILURE)
				WebUI.click(findTestObject('Application/EnrollmentAndCareteamWizard/ProgramWizard/EACW_PW_DeleteProgramPopupDeleteButton'), FailureHandling.STOP_ON_FAILURE)
				WebUI.delay(3)
				WebUI.verifyElementPresent(findTestObject('Application/EnrollmentAndCareteamWizard/ProgramWizard/EACW_PW_SuccesfullDeletedPopupOkButton'), 30, FailureHandling.STOP_ON_FAILURE)
				WebUI.click(findTestObject('Application/EnrollmentAndCareteamWizard/ProgramWizard/EACW_PW_SuccesfullDeletedPopupOkButton'), FailureHandling.STOP_ON_FAILURE)
				WebUI.delay(2)
				if(!WebUI.verifyElementPresent(findTestObject('Application/EnrollmentAndCareteamWizard/ProgramWizard/EACW_PW_VerticalElipsisIcon'), 30, FailureHandling.OPTIONAL)){
					break
				}
			}
			if(!WebUI.verifyElementPresent(findTestObject('Application/EnrollmentAndCareteamWizard/ProgramWizard/EACW_PW_VerticalElipsisIcon'), 30, FailureHandling.OPTIONAL)){
				deleteProgramStatus="User deleted the the Programs"
			}else{
				deleteProgramStatus="More number of Programs listed in the Program Wizard page"
			}
		}catch(Exception e){
			deleteProgramStatus="User not able to deleted the program"
		}
	}
}
