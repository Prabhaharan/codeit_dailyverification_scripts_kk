package application

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import internal.GlobalVariable
import keywordsLibrary.CommomLibrary
import MobileBuiltInKeywords as Mobile
import WSBuiltInKeywords as WS
import WebUiBuiltInKeywords as WebUI

public class CarePlanApp {

	@Keyword
	public Boolean CarePlanAppNavigation(){
		try{
			WebUI.click(findTestObject('Application/CarePlanApp/AppcareplanIcon'), FailureHandling.STOP_ON_FAILURE)
			WebUI.switchToWindowIndex(1)
			WebUI.maximizeWindow()
			WebUI.delay(8)
			Boolean appNavigationStatus=WebUI.verifyElementPresent(findTestObject('Application/CarePlanApp/SearchPatientPage/CP_SPP_patientSearch'), 15, FailureHandling.OPTIONAL)
			return appNavigationStatus
		} catch(Exception e){
			return false
		}
	}
	@Keyword
	public Boolean PatientSearchFieldNavigation(){
		try{
			WebUI.click(findTestObject('Application/CarePlanApp/SearchPatientPage/CP_SPP_patientSearch'), FailureHandling.STOP_ON_FAILURE)
			WebUI.delay(10)
			Boolean PatientSearchFieldNavigationStatus=WebUI.verifyElementPresent(findTestObject('Application/CarePlanApp/SearchPatientPage/CP_SPP_patientID'), 15, FailureHandling.OPTIONAL)
			return PatientSearchFieldNavigationStatus
		} catch(Exception e){
			return false
		}
	}
	@Keyword
	public Boolean searchPatient(String patientID, String patientLastName){
		try{
			WebUiCommonHelper.findWebElement(findTestObject('Application/CarePlanApp/SearchPatientPage/CP_SPP_patientID'), 3).clear()
			WebUI.setText(findTestObject('Application/CarePlanApp/SearchPatientPage/CP_SPP_patientID'), patientID, FailureHandling.STOP_ON_FAILURE)
			WebUI.click(findTestObject('Application/CarePlanApp/SearchPatientPage/CP_SPP_lookUP'), FailureHandling.STOP_ON_FAILURE)
			//Verify Patient searched patient
			WebUI.delay(6)
			TestObject patientSearchObject=findTestObject('Base/commanXpath')
			patientSearchObject.findProperty('xpath').setValue("(//table[@class='dataVal'])[3]//td[contains(text(),'"+patientLastName+"')]")
			Boolean searchPatientStatus=WebUI.verifyElementPresent(patientSearchObject, 15, FailureHandling.OPTIONAL)
			return searchPatientStatus
		} catch(Exception e){
			return false
		}
	}
	@Keyword
	public Boolean searchPatientInCarePlanAppwithPatientID(String patientID){
		try{
			//Search patient with PatientLastName and Patient ID
			WebUI.click(findTestObject('Application/DemographicsApp/SearchPatientPage/DA_SPP_searchButton'), FailureHandling.STOP_ON_FAILURE)
			WebUI.delay(4)
			TestObject patientSearchObject = findTestObject('Base/commanXpath')
			patientSearchObject.findProperty('xpath').setValue("//div[text()='" +patientID+"']")
			Boolean patietSearchStatus=WebUI.verifyElementPresent(patientSearchObject, 15, FailureHandling.OPTIONAL)
			WebUI.delay(3)
			WebUI.click(patientSearchObject, FailureHandling.STOP_ON_FAILURE)
			WebUI.delay(3)
			return patietSearchStatus
		} catch(Exception e){
			return false
		}
	}
	@Keyword
	public Boolean searchPatient(String patientID){
		try{
			WebUiCommonHelper.findWebElement(findTestObject('Application/CarePlanApp/SearchPatientPage/CP_SPP_patientID'), 3).clear()
			WebUI.setText(findTestObject('Application/CarePlanApp/SearchPatientPage/CP_SPP_patientID'), patientID, FailureHandling.STOP_ON_FAILURE)
			Boolean searchPatientStatus=WebUI.click(findTestObject('Application/CarePlanApp/SearchPatientPage/CP_SPP_lookUP'), FailureHandling.STOP_ON_FAILURE)
			//Verify Patient searched patient
			WebUI.delay(6)
			return searchPatientStatus
		} catch(Exception e){
			return false
		}
	}
}
