package application

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords

import internal.GlobalVariable

import MobileBuiltInKeywords as Mobile
import WSBuiltInKeywords as WS
import WebUiBuiltInKeywords as WebUI

public class CareBookUniversalSearch {

	@Keyword
	public Boolean careBookUniversalSearchInputField(String patientFirstName,String patientLastName){
		try{
			// First Name and Last name as input
			WebUI.delay(2)
			WebUI.clearText(findTestObject("Object Repository/Application/CareBook/CB_Search_FirstName"),FailureHandling.STOP_ON_FAILURE)
			WebUI.sendKeys(findTestObject('Object Repository/Application/CareBook/CB_Search_LastName'), patientLastName)
			WebUI.delay(2)
			WebUI.sendKeys(findTestObject("Object Repository/Application/CareBook/CB_Search_FirstName"), patientFirstName)
			WebUI.click(findTestObject("Object Repository/Application/CareBook/CareBookUniversalSearch/CBUS_Icon"))

			Boolean careBookUniversalWindowopenStatus=WebUI.verifyElementPresent(findTestObject("Object Repository/Application/CareBook/CareBookUniversalSearch/CBUS_Close"), 20, FailureHandling.OPTIONAL)
			return careBookUniversalWindowopenStatus
		} catch(Exception e){
			return false
		}
	}
}