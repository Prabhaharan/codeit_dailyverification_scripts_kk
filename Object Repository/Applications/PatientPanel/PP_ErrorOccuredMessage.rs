<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>PP_ErrorOccuredMessage</name>
   <tag></tag>
   <elementGuidId>08c0b9a2-ed75-42ea-be47-8a3f6d0dc40b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//h1[contains(text(),'Error Occurred')]</value>
   </webElementProperties>
</WebElementEntity>
