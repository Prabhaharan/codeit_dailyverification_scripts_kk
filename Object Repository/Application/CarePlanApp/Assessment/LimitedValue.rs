<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>LimitedValue</name>
   <tag></tag>
   <elementGuidId>5544fc9b-c31f-42cb-958d-b5dbcd1ed84c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//option[text()='Limited ']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//option[text()='Limited ']</value>
   </webElementProperties>
</WebElementEntity>
