<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>existingGoalCheck</name>
   <tag></tag>
   <elementGuidId>a327eaba-25fb-44ae-8f58-94dcf5d40e88</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//td[contains(text(),&quot;Goals&quot;)and not(contains(text(),&quot;(0)&quot;))]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//td[contains(text(),&quot;Goals&quot;)and not(contains(text(),&quot;(0)&quot;))]</value>
   </webElementProperties>
</WebElementEntity>
