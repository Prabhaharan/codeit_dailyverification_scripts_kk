<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>CP_ProgressNote_CreateButton</name>
   <tag></tag>
   <elementGuidId>12d2d69f-076f-4777-a8c8-a5587ff09745</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//input[@value=&quot;Create&quot; and @type=&quot;button&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//input[@value=&quot;Create&quot; and @type=&quot;button&quot;]</value>
   </webElementProperties>
</WebElementEntity>
