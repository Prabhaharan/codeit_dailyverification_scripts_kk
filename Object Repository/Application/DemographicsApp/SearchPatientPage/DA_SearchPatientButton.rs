<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>DA_SearchPatientButton</name>
   <tag></tag>
   <elementGuidId>fea4916c-5c28-4be0-93f5-a6b04c07e5cc</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//td[text()='Search Pati']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//td[contains(text(),'Search')]</value>
   </webElementProperties>
</WebElementEntity>
