<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>MS_SP_DelegationEmailInputField</name>
   <tag></tag>
   <elementGuidId>c8a3b597-0420-4247-899b-c74d4ed7193a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//td/div[text()='Delegate Email :']//following::td/input[@type='text']</value>
   </webElementProperties>
</WebElementEntity>
