<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>EACW_PW_SimplifiedProgramConsentPopup</name>
   <tag></tag>
   <elementGuidId>c8ecd7b3-f09e-4189-8b35-0de0dd907584</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//p[contains(text(),&quot;Consent is required for this program.&quot;)]//ancestor::md-dialog/md-dialog-actions/button/span[contains(text(),&quot;Cancel&quot;)]</value>
   </webElementProperties>
</WebElementEntity>
