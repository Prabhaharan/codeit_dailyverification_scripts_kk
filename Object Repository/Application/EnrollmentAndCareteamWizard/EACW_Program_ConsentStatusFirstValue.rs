<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>EACW_Program_ConsentStatusFirstValue</name>
   <tag></tag>
   <elementGuidId>c9898e3d-b975-40ab-8399-7e217c33d30d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//md-option[@ng-repeat=&quot;opt in programConsentPanel.app.consentList&quot;][1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//md-option[@ng-repeat=&quot;opt in programConsentPanel.app.consentList&quot;][1]</value>
   </webElementProperties>
</WebElementEntity>
