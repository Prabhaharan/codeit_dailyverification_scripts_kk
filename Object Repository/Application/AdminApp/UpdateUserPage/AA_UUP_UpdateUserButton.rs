<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>AA_UUP_UpdateUserButton</name>
   <tag></tag>
   <elementGuidId>3d89d445-4198-4fb0-971f-092ba4eeeb10</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@eventproxy=&quot;AdminUpdateUser&quot; and @role=&quot;button&quot;]</value>
   </webElementProperties>
</WebElementEntity>
