<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>AA_Clear_LastName</name>
   <tag></tag>
   <elementGuidId>4c9da1c5-3ea4-4bae-bbf0-c4c6f93a1bbb</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//label[contains(text(),'Last Name')]//parent::nobr/..//following-sibling::td/input</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//label[contains(text(),'Last Name')]//parent::nobr/..//following-sibling::td/input</value>
   </webElementProperties>
</WebElementEntity>
