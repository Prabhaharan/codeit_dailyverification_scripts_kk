<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>AA_RedLAstName</name>
   <tag></tag>
   <elementGuidId>64ef65f7-8a3c-462c-a7fd-13d3a4bf9ba5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//label[contains(text(),'Last Name')]//parent::nobr/..//following-sibling::td//following::input[@name=&quot;isc_TextItem_12&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//label[contains(text(),'Last Name')]//parent::nobr/..//following-sibling::td//following::input[@name=&quot;isc_TextItem_12&quot;]</value>
   </webElementProperties>
</WebElementEntity>
