<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>AA_UUP_SaveSuccessfulOKButton</name>
   <tag></tag>
   <elementGuidId>739212f8-6d55-4bec-a8dd-c14937230974</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//td[text()='Save successful.']//following::div[text()='OK']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//td[text()='Save successful.']//following::div[text()='OK']</value>
   </webElementProperties>
</WebElementEntity>
