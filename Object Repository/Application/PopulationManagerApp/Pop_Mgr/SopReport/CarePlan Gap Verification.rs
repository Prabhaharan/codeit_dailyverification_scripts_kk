<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>CarePlan Gap Verification</name>
   <tag></tag>
   <elementGuidId>8729a2a8-02d9-4069-9fad-bf91ee36154f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>(//td[text()='Patient ID'][@valign='middle']/../following-sibling::tr[1]/td)[1]</value>
   </webElementProperties>
</WebElementEntity>
