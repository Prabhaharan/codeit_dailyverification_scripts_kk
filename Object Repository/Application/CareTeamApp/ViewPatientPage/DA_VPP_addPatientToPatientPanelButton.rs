<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>DA_VPP_addPatientToPatientPanelButton</name>
   <tag></tag>
   <elementGuidId>798d9b0e-1cce-4aae-8da4-ab70c48c95c0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[contains(text(),'Add To Patient Panel')]</value>
   </webElementProperties>
</WebElementEntity>
