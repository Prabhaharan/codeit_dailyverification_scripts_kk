<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>CTA_SPP_CTS_CareTeamSection</name>
   <tag></tag>
   <elementGuidId>20fca0a4-c4f3-44a2-a26f-349f2583d95a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[contains(text(),&quot;Care Team&quot;)]//parent::td[@class=&quot;sectionHeaderclosed&quot;]</value>
   </webElementProperties>
</WebElementEntity>
