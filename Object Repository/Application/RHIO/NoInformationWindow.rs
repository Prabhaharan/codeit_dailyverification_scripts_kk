<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>NoInformationWindow</name>
   <tag></tag>
   <elementGuidId>0424958d-402d-4ea4-8a37-195984ffad0a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[normalize-space(text())='No RHIO Information Sharing configuration is available.']</value>
   </webElementProperties>
</WebElementEntity>
